# Servidor

Por el momento estas instrucciones están disponibles para sistemas basados en distrubuciones de GNU/Linux como [Debian](https://www.debian.org/), [Ubuntu](https://ubuntu.com/) etc.

Para realizar una transmisión es necesario tener un servidor (instrucciones sugeridas para configurar un servidor virtual se encuentran en servidorVirtual.md), punto de montaje (Icecast) y un programa transmisor (butt).

## Punto de montaje

El primer paso consiste en configurar un punto de montaje, es decir, un lugar hacia el que vamos a enviar la señal de audio y que podrá ser sintonizada por cualquier persona por medio de la web. 

Al momento de escritura, este documento utiliza y se adapta a un droplet de 5 dll en [Digital Ocean](https://www.digitalocean.com/) pero funciona en cualquier servidor público basado en Debian. 

También es posible obtener puntos de montaje con organizaciones no lucrativas como [giss.tv](https://giss.tv/)

Este conjunto de instrucciones utiliza un servidor personal para la configuración de [Liquidsoap](https://www.liquidsoap.info/).

Antes de iniciar es importante conocer algo de sistemas basados en UNIX y en específico, GNU/Linux. En terminal.md se puede encontrar algo de información para el trabajo con terminal y la instalación de paquetes. 

## Icecast

El primer paso consiste en instalar [Icecast](https://icecast.org/):

``sudo apt-get install icecast2``

Es posible que se despliegue un menú de configuración de icecast para determinar contraseñas, por favor, no dejes las contraseñas que vienen por defecto. 

Una vez que ya está instalado, el archivo de configuración se puede editar con: 

``sudo nano /etc/icecast2/icecast.xml``

Un ejemplo de un archivo de configuración vacío se encuentra en icecast/icecast.xml (similar al que viene por defecto pero con algunas anotaciones en castellano)

### Servidor local 

Para iniciar icecast es necesario ejecutar en una terminal: 

``sudo systemctl start icecast2``

Para detener: 

``sudo systemctl stop icecast2``

### Servidor virtual 

La publicación del punto de montaje requiere algunos pasos adicionales. 

Primero, tener un dominio asociado a la dirección de un servidor virtual. En servidorVirtual.md se explican los pasos para generar un servidor y asociar dominios específicos a ese servidor. 

Para tener el punto de montaje del servidor es necesario configurar el firewall para permitir señales entrantes. 

``
sudo iptables -A INPUT -p tcp --dport 8000 -j ACCEPT
``

``
sudo iptables-save
``

En mi caso fue necesario hacerlo desde iptables-legacy: 

``sudo iptables-legacy -A INPUT -p tcp --dport 8000 -j ACCEPT``

Y con eso es suficiente para establecer el punto de montaje. 

Importante: si el servidor está asociado a un dominio o a una página, por defecto va a funcionar en http y no en https. Queda pendiente la redirección de https a http. 

## Transmisión

Para revisar las instrucciones de transmisión, revisar [cliente.md](./cliente.md)

Con la configuración de icecast que aparece en este documento es posible activar distintas transmisiones. Esto será importante si el objetivo de la radio es alternar entre una playlist y una transmisión en vivo. 

También se pueden generar distintos puntos de montaje para distintos objetivos. Si este fuera el caso, sería importante revisar las características del servidor y la cantidad de radio escuchas. 

