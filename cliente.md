# Cliente

La transmisión principal se realiza desde el mismo servidor con liquidsoap. En [liquidsoap.md](./liquidsoap.md) se pueden encontrar algunas referencias para realizar esto y para modificar el archivo [radio.liq](./liquidsoap/radio.liq)


Con la configuración de este manual es posible arrancar una transmisión continua con una playlist en liquidsoap que se interrumpa cada vez que haya una transmisión en vivo. 

El punto de montaje puede tener cualquier nombre excepto stream que es el nombre del stream principal, esto quiere decir que existiran dos streams, uno que tendrá el punto de montaje público y otro que servirá para fines internos es decir para alternar la playlist de liquidsoap y una transmisión en vivo pero que también podría ser compartido. 

El archivo de este documento utiliza el punto de montaje **/stream** para la transmisión principal y el punto de montaje **/live** para enviar una transmisión en vivo. 

El punto de montaje puede realizarse con butt o con alguna interfaz gráfica de usuario personalizada. 
