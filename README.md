# web-radio

Instrucciones y recursos en castellano para montar un sistema de transmisión de audio. 

Este respositorio surge de la necesidad de concentrar información sobre cómo montar y transmitir una señal de audio en la web. 

## Cliente

Para transmitir hacia una instancia de estas instrucciones es necesario consultar [cliente.md](./cliente.md)

## Servidor de Icecast

Para montar un sistema de transmisión es necesario consultar [servidorIcecast.md](./servidorIcecast.md)

También se pueden consultar algunas sugerencias para trabajar con [terminal.md](./terminal.md)

## Servidor Virtual

Los pasos para configurar un servidor virtual en un servicio como Digital Ocean se pueden encontrar en: [servidorVirtual.md](./servidorVirtual.md)

## Tips y recomendaciones

Un conjunto de tips y recomendaciones se pueden encontrar en [tips.md](./tips.md)

## Recursos

- Debian
- Icecast
- LiquidSoap


