# Liquidsoap

Es posible utiliza LiquidSoap para configurar una transmisión continua de listas, de streams o una mezcla de ambos. 

Queda pendiente actualizar la versión de Liquidsoap

Consultar el archivo ./liquidsoap/radio.liq para más detalles de configuración 

Para ejecutar liquid soap en un servidor sin asociarlo a algún daemon se puede usar screen. 

Instalar screen;

``sudo apt-get install screen``

Nueva sesión de screen:

``screen -S liquidsoap``

Ejecutar liquidsoap 

``liquidsoap /ruta/al/archivo.liq``

Ctrl + A y luego D para detach la sesión de screen. Esto no detiene liquidsoap, seguirá funcionando en segundo plano incluso si sucede una descionexión del servidor. 

Para retomar la sesión: 

``screen -r liquidsoap``

Para revisar el estado de las sesiones: 

``screen -ls``

Para cambiar o actualizar un archivo es necesario retomar la sesión, detener liquidsoap con ctrl+c, hacer los cambios y finalmente volver a correr liquidsoap.

